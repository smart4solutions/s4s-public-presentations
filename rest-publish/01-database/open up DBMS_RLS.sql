create or replace public synonym dbms_rls for sys.dbms_rls;

grant execute on sys.dbms_rls to RESTPUBL_ORDS_PRS;

grant execute on sys.dbms_session to RESTPUBL_ORDS_PRS;

--grant execute on dbms_rls to RESTPUBL_CORE_PRS;
