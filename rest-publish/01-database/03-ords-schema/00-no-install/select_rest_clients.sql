select * from USER_ORDS_ROLES;              -- shows roles

select * from USER_ORDS_PRIVILEGES;         -- shows privileges

select * from USER_ORDS_PRIVILEGE_ROLES;    -- Relationship between privs and roles

select * from USER_ORDS_PRIVILEGE_MAPPINGS; -- What patterns are protected by what privilege

select * from user_ords_client_privileges;

select * from USER_ORDS_CLIENTS;            -- Client info

select * from USER_ORDS_CLIENT_PRIVILEGES;  -- Client to priv mapping

select * from USER_ORDS_CLIENT_ROLES ctrl;  -- Client to role mapping

select ocl.client_id
      ,ocl.client_secret
      ,ocl.name as client_name
      ,listagg(distinct ocr.role_name, ':') as role_names
      ,listagg(distinct opm.pattern, ', ') as patterns
from   user_ords_clients ocl
  left join user_ords_client_roles ocr on ocr.client_id = ocl.id
  left join user_ords_privilege_roles opr on opr.role_id = ocr.role_id
  left join user_ords_privilege_mappings opm on opm.privilege_id = opr.privilege_id
group by ocl.client_id, ocl.client_secret, ocl.name
order by role_names;

select * from logger_logs l
where 1=1
--  and l.user_name != 'S4SBO_CORE_DEV'
  and l.module not like 'S4SBO%'
  and l.scope not like 's4s_exact%'
order by l.id desc;
