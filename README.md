![Smart4solutions](https://smart4solutions.nl/wp-content/uploads/2020/05/smart4solutions-logo-620.png)

# SMART4Solutions Public Presetations
We use this bitbucket area to store our public presentations. Most of these will be from the conference you visited.

Each of the folders will have a prsentation-file taken  form the powerpoint in PDF format. Some folders will have an APEX export or some other SQL or scripts.
Some of them might have an additional readme.md with some instructions or last minute changes the author wants to share.

We hope you enjoyed our presetation and look forward to you remarks or questions.

Kind regards,
Richard Martens
member of the warmest APEX family in the Netherlands

---

### Table of contents

1. **So Now What**: or what to do after a Social Login on your APEX application
2. **Utilizing the data attribute**: On how to hook javascript functions to specific rendered HTML elements
3. **Classic Report the power of no template**: Contains some tips on using the Classic Report with some really complicated HTML structures
4. **PL/SQL OO**: Describes an object oriented approach to table API's. it introduces basic OO concepts within PL/SQL